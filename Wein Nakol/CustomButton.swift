//
//  CustomButton.swift
//  Wein Nakol
//
//  Created by Mac on 3/15/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = true;
    }

}
