//
//  ViewController.swift
//  Wein Nakol
//
//  Created by Mac on 3/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(showVC), userInfo: nil, repeats: false)

       
    }
    
    @objc func showVC(){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SuggestViewController") as! SuggestViewController
        self.present(myVC, animated: true, completion: nil)
    }


}

